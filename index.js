const express = require('express');
const app = express();

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const PORT = 4001;
const axios=require('axios');
const API_KEY = 'AIzaSyCQMl1toeEIaveuicFD_O8Wh_u2wU7rmSE';
const authRoutes = require("./Routes/Auth")
const blogRoutes = require("./Routes/Blog");
const imageuploadRoutes = require('./Routes/imageUploadRoutes');
const User = require("./Models/UserSchema")
require('./db')
require('dotenv').config();

app.use(bodyParser.json());
const allowedOrigins = ['http://localhost:3000']; // Add more origins as needed
//Configure CORS with credentials
app.use(
  cors({
      origin: function (origin, callback) {
          if (!origin || allowedOrigins.includes(origin)) {
              callback(null, true);
          } else {
              callback(new Error('Not allowed by CORS'));
          }
      },
      credentials: true, // Allow credentials
  })
);
app.use(cookieParser());

app.use('/auth', authRoutes);
app.use('/blog', blogRoutes);
app.use('/image', imageuploadRoutes);


//function to get categories
app.get('/blogcategories', async (req, res) => {
  const blogCategories = [
      "Technology Trends",
      "Health and Wellness",
      "Travel Destinations",
      "Food and Cooking",
      "Personal Finance",
      "Career Development",
      "Parenting Tips",
      "Self-Improvement",
      "Home Decor and DIY",
      "Book Reviews",
      "Environmental Sustainability",
      "Fitness and Exercise",
      "Movie and TV Show Reviews",
      "Entrepreneurship",
      "Mental Health",
      "Fashion and Style",
      "Hobby and Crafts",
      "Pet Care",
      "Education and Learning",
      "Sports and Recreation"
  ];
  res.json(
      {
          message: 'Categories fetched successfully',
          categories: blogCategories
      }
  )
})

// Function to search for YouTube channels
async function searchChannels(query) {
    const { searchTerm, maxResults, countryCode, minSubscriberCount } = query;
    try {
      const searchResponse = await axios.get('https://www.googleapis.com/youtube/v3/search', {
        params: {
          key: API_KEY,
          q: searchTerm || 'cooking',
          type: 'channel',
          regionCode: countryCode || 'US',
          maxResults: Math.max(maxResults || 50, 10),
          part: 'snippet',
        },
      });
  
      const channels = [];
  
      for (const searchResult of searchResponse.data.items) {
        const channelId = searchResult.id.channelId;
        const channelTitle = searchResult.snippet.title;
  
        // Retrieve channel statistics
        const channelResponse = await axios.get('https://www.googleapis.com/youtube/v3/channels', {
          params: {
            key: API_KEY,
            id: channelId,
            part: 'statistics,snippet',
          },
        });
  
        if (channelResponse.data.items) {
          const channelDetails = channelResponse.data.items[0];
          const statistics = channelResponse.data.items[0].statistics;
          const subscriberCount = parseInt(statistics.subscriberCount, 10);
          const defaultProfilePictureUrl = channelDetails.snippet.thumbnails.default.url;
  
          // Check if the channel has a high subscriber count
          if (subscriberCount >= minSubscriberCount) {
            channels.push({
              title: channelTitle,
              channelId: channelId,
              subscriberCount: subscriberCount,
              description: channelDetails.snippet.description,
              profileUrl:defaultProfilePictureUrl,
              statistics: statistics
            });
          }
        }
      }
  
      return channels;
    } catch (error) {
      console.error('Error searching for channels:', error);
      throw error;
    }
  }

app.get('/high-subscriber-channels', async(req, res) => {
    try {
        const searchTerm = req.query.searchTerm;
        const maxResults = req.query.maxResults;
        const countryCode = req.query.countryCode;
        const minSubscriberCount = req.query.minSubscriberCount;
    
        const channels = await searchChannels({
          searchTerm,
          maxResults,
          countryCode,
          minSubscriberCount
        });
    
        if (channels.length > 0) {
          res.json({ channels });
        } else {
          res.json({ message: 'No high subscriber count channels found.' });
        }
      } catch (error) {
        res.status(500).json({ error: 'An error occurred while fetching channels.' });
      }
    // res.json({ message: 'The API is working' });
});


app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
