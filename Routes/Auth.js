const express = require('express');
const router = express.Router();
const User = require('../Models/UserSchema')
const errorHandler = require('../Middlewares/errorMiddleware');
const authTokenHandler = require('../Middlewares/checkAuthToken');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');

// phhn gxvz qxkk jhib

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'ku.arajay.ka53@gmail.com',
        pass: 'phhngxvzqxkkjhib'
    }
})

router.get('/test', async (req,res) => {
    res.json({
        message: "Auth api is Working"
    })
})

//next is nothing but middleware
router.post('/sendotp', async (req, res) => {
    const { email } = req.body;
    const otp = Math.floor(100000 + Math.random() * 900000);
    try {
        const mailOptions = {
            from: process.env.COMPANY_EMAIL,
            to: email,
            subject: 'OTP for verification of Promate',
            text: `Your OTP for verification is ${otp}`,
        }

        transporter.sendMail(mailOptions, async (err, info) => {
            if (err) {
                console.log(err);
                res.status(500).json(createResponse(false, err.message));
            } else {
                res.json({ message: 'otp success', otp: otp });
            }
        });

    } catch (err) {
        next(err);
    }
});

router.post('/register', async (req, res, next) => {
    try {
        const { name, email, password } = req.body;
        const existingUser = await User.findOne({ email: email }).maxTimeMS(30000);

        if (existingUser) {
            return res.status(409).json({ success: false, message: 'Email already exists' });
        }

        const newUser = new User({
            name,
            password,
            email,
        });

        await newUser.save(); // Await the save operation

        res.status(201).json({ success: true, message: 'User registered successfully' });
    } catch (err) {
        // Pass the error to the error middleware
        next(err);
    }
});

router.post('/login', async (req, res, next) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email });

        if(!user) {
            return res.status(400).json({ message: 'Invalid Email or Password' });
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if(!isMatch) {
            return res.status(400).json({ message: 'Invalid Credentials' });
        }

        const authToken = jwt.sign({ userId: user._id }, process.env.JWT_SECRET_KEY, { expiresIn: '1000d' });
        const refreshToken = jwt.sign({ userId: user._id }, process.env.JWT_REFRESH_SECRET_KEY, { expiresIn: '1000d' });

        res.cookie('authToken', authToken, { httpOnly: true });
        res.cookie('refreshToken', refreshToken, { httpOnly: true });
        res.status(200).json({ message:'Login successful' });

    } catch (err) {
        next(err);
    }
})

// Route to handle sending emails
router.post('/send-email', (req, res) => {
    const { name, email, creator_name, budget, platform, location, type, tenure_period } = req.body;
  
    // Email options
    const mailOptions = {
      from: 'your-email@gmail.com', // Sender email address
      to: email, // Receiver email address
      subject: 'Message from Your Promate', // Subject line
      text: `Dear ${name},\n\nThank you for contacting us. Your message has been received.\n\n 
            Creator Name : ${creator_name}
            Platform : ${platform}
            Budget : ${budget}
            Location: ${location}
            Promotion Types: ${type}
            Tenure Period: ${tenure_period} ` // Plain text body
    };
  
    // Send email
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error('Error sending email:', error);
        res.status(500).json({ message: 'Failed to send email' });
      } else {
        console.log('Email sent:', info.response);
        res.status(200).json({ message: 'Email sent successfully' });
      }
    });
  });

router.use(errorHandler)

router.get('/checklogin', async (req, res) => {
    res.json({
        ok: true,
        message: 'User authenticated successfully'
    })
})


module.exports = router;
